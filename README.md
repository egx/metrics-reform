# Metrics Reform

A discussion about how we can reduce the damage done by poorly chosen and badly measured metrics in business, schooling, and government.

## Background

Metrics play an increasingly important role in setting policy and guiding institutions, especially since the "data driven" boom in the 2000's. While making decisions informed by data is the cornerstone of science, there are many ways that a "data driven" approach can actually lead to more harm than good.

### Ways metrics go wrong

For instance, there are three main aspects of metrics collection and usage that must all be done correctly to get useful data for decision making. Any one of these done incorrectly or carelessly can make the "insights" useless or dangerous.

Are you:

1. Collecting the right metrics?
2. Measuring them correctly or introducing errors?
3. Analyzing them in a way the brings out meaning?

## Goals

The ultimate aim of this discussion is to provide a set of recommendations and best practices that will allow all orgs to successfully deploy metrics to assist with decision making in a way that improves outcomes without falling prey to the common trap of blindly following the metrics.

In order to achieve this, we will need to address the following questions and issues:

* What are the issues with data acquisition?
	* e.g. methodology
* Where is there room for improvement? What does that look like?
	* Improving surveys
	* Changing how data is acquired
* How does data acquisition go wrong?
	* When this goes wrong what are the why and how?
	* What are the consequences?
* What industries would have the most ROI on making changes?
	* Which industries have the most ability to make changes?
	* Which will benefit the most?

## Discussion Points

* Context integration/flattening
* Feedback systems e.g. GoodHart's Law, the Cobar Effect
* Cheap vs accurate metrics
* Optimal balance between data-driven and logic/wisdom driven decision making

## References

### Con

* [Metrics Fetishism - Chris Hecker's Website](http://www.chrishecker.com/Metrics_Fetishism)
* [Every attempt to manage academia makes it worse | Sauropod Vertebra Picture of the Week](https://svpow.com/2017/03/17/every-attempt-to-manage-academia-makes-it-worse/)
* [When targets and metrics are bad for business](https://thehustle.co/Goodharts-Law)
* [Against metrics: how measuring performance by numbers backfires | Aeon Ideas](https://aeon.co/ideas/against-metrics-how-measuring-performance-by-numbers-backfires)
* [Muller, J.: The Tyranny of Metrics (Hardcover) | Princeton University Press](https://press.princeton.edu/titles/11218.html)
* [Fixing Metric Fixation: A Review of The Tyranny of Metrics – in All things](https://inallthings.org/fixing-metric-fixation-a-review-of-the-tyranny-of-metrics/)
* [Making metrics matter - Clubhouse](https://clubhouse.io/blog/making-metrics-matter-bfaddd417b0b/)
* [Which metrics matter? - DevRel.net DevRel.net](https://devrel.net/strategy-and-metrics/which-metrics-matter)
* [The Quantified Welp: Measuring an Activity Makes Us Enjoy It Less - The Atlantic](https://www.theatlantic.com/technology/archive/2016/02/the-quantified-welp/470874/)
* [What happens to police departments that collect more fines? They solve fewer crimes. - The Washington Post](https://www.washingtonpost.com/news/monkey-cage/wp/2018/09/24/want-your-police-department-to-collect-more-fines-it-will-solve-fewer-crimes/?noredirect=on)
* [Could Removing Social Media Metrics Reduce Polarization?](https://onezero.medium.com/could-removing-social-media-metrics-reduce-polarization-4d604352c705)

### Pro

* [You may hate metrics. But they’re making journalism better. - Columbia Journalism Review](https://www.cjr.org/innovations/you-may-hate-metrics-guardian-audience-twitter-images.php)

### Statistical Illusions

These are relevant because they are ways that metrics can be misinterpreted even if no one is actually "messing up" or doing it wrong.

* [The Inspection Paradox is Everywhere - Towards Data Science](https://towardsdatascience.com/the-inspection-paradox-is-everywhere-2ef1c2e9d709)
